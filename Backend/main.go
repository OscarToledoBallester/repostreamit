package main

import (
	"golang.org/x/oauth2"
	"golang.org/x/net/context"
	"database/sql"
	"encoding/json"
	"fmt" 
	"io"  
	"io/ioutil"
	"log"      
	"net/http" 
	"strconv"
	"strings"
	"time"
	mux "github.com/gorilla/mux"

	jwt "github.com/dgrijalva/jwt-go" 
	_ "github.com/go-sql-driver/mysql"
)

type snetwork struct {
	ID          int
	Name        string
	URL         string
	Img         string
	Users       string
	Description string
}

type language struct {
	ID        int
	Iso       string
	Name      string
	Img       string
	Countries int
	Speakers  string
}

type country struct {
	ID     int
	Iso    string
	Name   string
	Img    string
	People string
}

type company struct {
	ID   int
	Name string
	Img  string
}

type genre struct {
	ID        int
	Name      string
	ShortName string
}

type platform struct {
	ID   int
	Name string
	Img  string
	Date string
}

type gameRelease struct {
	Platform  platform
	Developer company
	Publisher company
	Date      string
}

type game struct {
	ID       int
	Name     string
	Img      string
	Genres   []genre
	Releases []gameRelease
}

type streamer struct {
	Name           string
	Img            string
	Online         bool
	Subscriptions  int
	Viewers        int
	SocialNetworks snetwork
}

type apiConnection struct {
	ClientID 			string
	ClientSecret		string
	Token				string
	UrlToken			string
	UrlAuthentication	string
	UrlCallback			string
	UrlBasic			string
	UrlSearch			string
	UrlChannel			string
	UrlStatistics		string
	UrlTop				string
}

var mySigningKey = []byte("clavePrivada1234567890")

//Youtube Credentials
var youtube = apiConnection {
	ClientID 		: "<CLIENT ID>",
	ClientSecret 	: "<CLIENT SECRET>",
	UrlCallback 	: "http://localhost",
	UrlBasic 		: "https://www.googleapis.com/youtube/v3",
	UrlSearch 		: "https://www.googleapis.com/youtube/v3" + "/search",
	UrlChannel 		: "https://www.googleapis.com/youtube/v3" + "/search",
	UrlStatistics 	: "https://youtubeanalytics.googleapis.com/v2/reports",
	UrlTop 			: "https://www.googleapis.com/youtube/v3" + "/search",
}

//Mixer Credentials
var mixer = apiConnection {
	ClientID 			: "<CLIENT ID>",
	ClientSecret 		: "<CLIENT SECRET>",
	UrlBasic 			: "https://mixer.com/api/v1",
	UrlAuthentication 	: "https://mixer.com/oauth/authorize",
	UrlToken 			: "https://mixer.com/api/v1" + "/oauth/token",
	UrlCallback 		: "http://localhost",
	UrlSearch 			: "https://mixer.com/api/v1" + "/channels/",
	UrlChannel 			: "https://mixer.com/api/v1" + "/channels/",
	UrlStatistics 		: "https://mixer.com/api/v1" + "/channels/",
	UrlTop 				: "https://mixer.com/api/v1" + "/suggestions/channels",
}

//Twitch Credentials
var twitch = apiConnection {
	ClientID 			: "<CLIENT ID>",
	ClientSecret 		: "<CLIENT SECRET>",
	UrlBasic 			: "https://api.twitch.tv/helix",
	UrlAuthentication 	: "https://id.twitch.tv/oauth2/validate",
	UrlToken 			: "https://id.twitch.tv",
	UrlCallback 		: "http://localhost",
	UrlSearch 			: "https://api.twitch.tv/helix"+ "/streams",
	UrlChannel 			: "https://api.twitch.tv/helix" + "/users",
	UrlTop 				: "https://api.twitch.tv/helix" + "/streams",
}




func handleRequests() {
	//http.Handle("/", isAuthorized(homePage))
	//log.Fatal(http.ListenAndServe(":9000", nil))

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", funcList)

	router.HandleFunc("/login", login)

	router.HandleFunc("/snetworks", getAllSNetworks).Methods("GET")
	router.HandleFunc("/languages", getAllLanguages).Methods("GET")
	router.HandleFunc("/countries", getAllCountries).Methods("GET")
	router.HandleFunc("/companies", getAllCompanies).Methods("GET")
	router.HandleFunc("/platforms", getAllPlatforms).Methods("GET")
	router.HandleFunc("/genres", getAllGenres).Methods("GET")
	router.HandleFunc("/games", getAllGames).Methods("GET")
	router.HandleFunc("/top", getTopStreams).Methods("GET")
	router.HandleFunc("/search", search).Methods("GET")

	router.HandleFunc("/pruebaconexion", connectionTest)

	direccion := ":8080" // Como cadena, no como entero; porque representa una dirección
	fmt.Println("Servidor listo escuchando en " + direccion)
	log.Fatal(http.ListenAndServe(direccion, router))

}

func funcList(w http.ResponseWriter, peticion *http.Request) {
	fmt.Fprintf(w, "Welcome to StreamIT!")
}

//----- SQL -----//
func CreateCon() *sql.DB {

	dbDriver := "mysql"
	dbUser := "influencer"
	dbPass := "influencer5012"
	dbAddress := "tcp(127.0.0.1:3306)"
	dbName := "influent_bd"

	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@"+dbAddress+"/"+dbName)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("db is connected")
	}
	//defer db.Close()
	// make sure connection is available
	err = db.Ping()
	fmt.Println(err)
	if err != nil {
		fmt.Println("MySQL db is not connected")
		fmt.Println(err.Error())
	}
	return db
}

//----- AUTH -----//
func login(w http.ResponseWriter, peticion *http.Request) {
	user, existeUser := peticion.URL.Query()["user"]
	pass, existePass := peticion.URL.Query()["pass"]

	if existeUser && existePass {
		io.WriteString(w, "Hola, "+user[0]+" - "+pass[0])
		if true { //Aqui comprobamos si el usuario es correcto
			validToken, err := GenerateJWT()
			if err == nil {
				mapToken := map[string]string{"Token": validToken}
				mapRes, _ := json.Marshal(mapToken)
				io.WriteString(w, string(mapRes))
			} else {
				io.WriteString(w, "Error generando el token")
			}
		}
	} else {
		io.WriteString(w, "Hola, nos faltan datos")
	}
}

func isAuthorized(endpoint func(http.ResponseWriter, *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Header["Token"] != nil {

			token, err := jwt.Parse(r.Header["Token"][0], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("There was an error")
				}
				return mySigningKey, nil
			})

			if err != nil {
				fmt.Fprintf(w, err.Error())
			}

			if token.Valid {
				endpoint(w, r)
			}
		} else {

			fmt.Fprintf(w, "Not Authorized")
		}
	})
}

func GenerateJWT() (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["client"] = "Elliot Forbes"
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	tokenString, err := token.SignedString(mySigningKey)

	if err != nil {
		fmt.Errorf("Something Went Wrong: %s", err.Error())
		return "", err
	}

	return tokenString, nil
}

//----- EXAMPLE OF AJAX CONNECTION -----//
func connectionTest(w http.ResponseWriter, peticion *http.Request) {
	resB := ajaxConnectionTest()
	io.WriteString(w, string(resB))
}
func ajaxConnectionTest() (res string) {
	clienteHttp := &http.Client{}
	// Si quieres agregar parámetros a la URL simplemente haz una
	// concatenación :)
	url := "https://httpbin.org/get"
	peticion, err := http.NewRequest("GET", url, nil)
	if err != nil {
		// Maneja el error de acuerdo a tu situación
		log.Fatalf("Error creando petición: %v", err)

	}
	// Podemos agregar encabezados
	peticion.Header.Add("Content-Type", "application/json")
	peticion.Header.Add("X-Hola-Mundo", "Ejemplo")
	respuesta, err := clienteHttp.Do(peticion)
	if err != nil {
		// Maneja el error de acuerdo a tu situación
		log.Fatalf("Error haciendo petición: %v", err)
	}
	// No olvides cerrar el cuerpo al terminar
	defer respuesta.Body.Close()

	cuerpoRespuesta, err := ioutil.ReadAll(respuesta.Body)
	if err != nil {
		log.Fatalf("Error leyendo respuesta: %v", err)
	}

	respuestaString := string(cuerpoRespuesta)
	log.Printf("Código de respuesta: %d", respuesta.StatusCode)
	log.Printf("Encabezados: '%q'", respuesta.Header)
	contentType := respuesta.Header.Get("Content-Type")
	log.Printf("El tipo de contenido: '%s'", contentType)
	// Aquí puedes decodificar la respuesta si es un JSON, o convertirla a cadena
	log.Printf("Cuerpo de respuesta del servidor: '%s'", respuestaString)

	return respuestaString
}

//----- GET -----//
func getAllSNetworks(w http.ResponseWriter, peticion *http.Request) {
	db := CreateCon()
	results, err := db.Query("SELECT * FROM `snetworks`")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer db.Close()
	var res []snetwork
	for results.Next() {
		var sn snetwork
		// for each row, scan the result into our tag composite object
		err = results.Scan(&sn.ID, &sn.Name, &sn.URL, &sn.Img, &sn.Users, &sn.Description)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		res = append(res, sn)
	}

	resB, _ := json.MarshalIndent(res, " ", "\t")
	io.WriteString(w, string(resB))
}

func getAllLanguages(w http.ResponseWriter, peticion *http.Request) {
	db := CreateCon()
	results, err := db.Query("SELECT * FROM `languages`")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer db.Close()
	var res []language
	for results.Next() {
		var lang language
		// for each row, scan the result into our tag composite object
		err = results.Scan(&lang.ID, &lang.Name, &lang.Img, &lang.Countries, &lang.Speakers, &lang.Iso)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		res = append(res, lang)
	}

	resB, _ := json.MarshalIndent(res, " ", "\t")
	io.WriteString(w, string(resB))
}

func getAllCountries(w http.ResponseWriter, peticion *http.Request) {
	db := CreateCon()
	results, err := db.Query("SELECT * FROM `countries`")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer db.Close()
	var res []country
	for results.Next() {
		var count country
		// for each row, scan the result into our tag composite object
		err = results.Scan(&count.ID, &count.Iso, &count.Name, &count.Img, &count.People)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		res = append(res, count)
	}

	resB, _ := json.MarshalIndent(res, " ", "\t")
	io.WriteString(w, string(resB))
}

func getAllCompanies(w http.ResponseWriter, peticion *http.Request) {
	db := CreateCon()
	results, err := db.Query("SELECT * FROM `companies`")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer db.Close()
	var res []company
	for results.Next() {
		var count company
		// for each row, scan the result into our tag composite object
		err = results.Scan(&count.ID, &count.Name, &count.Img)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		res = append(res, count)
	}

	resB, _ := json.MarshalIndent(res, " ", "\t")
	io.WriteString(w, string(resB))
}

func getAllPlatforms(w http.ResponseWriter, peticion *http.Request) {
	db := CreateCon()
	results, err := db.Query("SELECT * FROM `platforms`")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer db.Close()
	var res []platform
	for results.Next() {
		var count platform
		// for each row, scan the result into our tag composite object
		err = results.Scan(&count.ID, &count.Name, &count.Img, &count.Date)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		res = append(res, count)
	}

	resB, _ := json.MarshalIndent(res, " ", "\t")
	io.WriteString(w, string(resB))
}

func getAllGenres(w http.ResponseWriter, peticion *http.Request) {
	db := CreateCon()
	results, err := db.Query("SELECT * FROM `genres`")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer db.Close()
	var res []genre
	for results.Next() {
		var count genre
		// for each row, scan the result into our tag composite object
		err = results.Scan(&count.ID, &count.Name, &count.ShortName)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		res = append(res, count)
	}

	resB, _ := json.MarshalIndent(res, " ", "\t")
	io.WriteString(w, string(resB))
}

func getAllGames(w http.ResponseWriter, peticion *http.Request) {
	db := CreateCon()
	results, err := db.Query("SELECT * FROM `games`")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer db.Close()
	var res []game
	for results.Next() {
		var count game
		// for each row, scan the result into our tag composite object
		err = results.Scan(&count.ID, &count.Name, &count.Img)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}

		//GENRES
		db := CreateCon()
		results2, err2 := db.Query("SELECT genres.id, genres.name, genres.short_name FROM `genres`, game_genres WHERE genres.id = game_genres.genre AND game_genres.game=" + strconv.Itoa(count.ID))
		if err2 != nil {
			panic(err2.Error()) // proper error handling instead of panic in your app
		}
		defer db.Close()
		var res2 []genre
		for results2.Next() {
			var count2 genre
			err2 = results2.Scan(&count2.ID, &count2.Name, &count2.ShortName)
			if err2 != nil {
				panic(err2.Error()) // proper error handling instead of panic in your app
			}
			res2 = append(res2, count2)
		}
		count.Genres = res2

		//RELEASES
		db2 := CreateCon()
		results3, err4 := db2.Query("SELECT platforms.id, platforms.name, platforms.img, dev.id, dev.name, dev.img, pub.id, pub.name, pub.img, games_platform.release_date FROM `games_platform`, companies AS dev, companies AS pub, platforms WHERE platforms.id = games_platform.platform AND dev.id = games_platform.developer AND pub.id = games_platform.publisher AND games_platform.game =" + strconv.Itoa(count.ID))
		if err4 != nil {
			panic(err4.Error()) // proper error handling instead of panic in your app
		}
		defer db2.Close()
		var res3 []gameRelease
		for results3.Next() {
			var count3 gameRelease
			var developer company
			var publisher company
			var platformR platform
			err4 = results3.Scan(&platformR.ID, &platformR.Name, &platformR.Img, &developer.ID, &developer.Name, &developer.Img, &publisher.ID, &publisher.Name, &publisher.Img, &count3.Date)
			if err4 != nil {
				panic(err4.Error()) // proper error handling instead of panic in your app
			}
			count3.Developer = developer
			count3.Platform = platformR
			count3.Publisher = publisher
			res3 = append(res3, count3)
		}
		count.Releases = res3

		res = append(res, count)
	}

	resB, _ := json.MarshalIndent(res, " ", "\t")
	io.WriteString(w, string(resB))
}

//----- SEARCH -----//
func search(w http.ResponseWriter, peticion *http.Request) {

	var res string = "Result/n "
	//Id de la social network
	if peticion.URL.Query()["sn"] != nil {
		var idSnetworks = peticion.URL.Query()["sn"]
		res += "Social Networks : " + strings.Join(idSnetworks, ",")
	}
	//String con los codigos de lenguage
	if peticion.URL.Query()["lg"] != nil {
		var idLanguages = peticion.URL.Query()["lg"]
		res += " - Languages : " + strings.Join(idLanguages, ",")
	}
	//String con los codigos de pais
	if peticion.URL.Query()["co"] != nil {
		var idCountries = peticion.URL.Query()["co"]
		res += " - Countries : " + strings.Join(idCountries, ",")
	}
	//Entero con las visitas minimas
	if peticion.URL.Query()["vsb"] != nil {
		var viewsBottom = peticion.URL.Query()["vsb"][0]
		res += " - Minimum Views : " + viewsBottom
	}
	//Entero con las visitas maximas
	if peticion.URL.Query()["vst"] != nil {
		var viewsTop = peticion.URL.Query()["vst"][0]
		res += " - Maximum Views : " + viewsTop
	}
	//Id de los juegos
	if peticion.URL.Query()["gm"] != nil {
		var idGames = peticion.URL.Query()["gm"]
		res += " - Game : " + strings.Join(idGames, ",")
	} else {
		//Id de los developers
		if peticion.URL.Query()["dev"] != nil {
			var developers = peticion.URL.Query()["dev"]
			res += " - Developers : " + strings.Join(developers, ",")
		}
		//Id de los publishers
		if peticion.URL.Query()["pub"] != nil {
			var publishers = peticion.URL.Query()["pub"]
			res += " - Publishers : " + strings.Join(publishers, ",")
		}
		//Id de los platforms
		if peticion.URL.Query()["plt"] != nil {
			var platforms = peticion.URL.Query()["plt"]
			res += " - Platforms : " + strings.Join(platforms, ",")
		}
		//Id de los genres
		if peticion.URL.Query()["ge"] != nil {
			var genres = peticion.URL.Query()["ge"]
			res += " - Genres : " + strings.Join(genres, ",")
		}
	}
	io.WriteString(w, string(res))
}

func searchByGame(games []game, snetworks []int, languages []string, countries []string, minimumViews int, maximumViews int) (res []streamer) {
	var streamers []streamer
	return streamers
}
func gamesByFilters(developers []company, publishers []company, platforms []platform, genres []genre, snetworks []snetwork, languages []string, countries []string, minimumViews int, maximumViews int) (res []game) {
	var games []game
	return games
}

//--------- API AUTHENTICATION ---------//
func getMixerToken() {

}
func getTwitchToken() {

}
func getYoutubeToken() {

}

//--------- TOP STREAMS ---------//
func getTopStreams(w http.ResponseWriter, peticion *http.Request) {
	var resB = mixerTopStreams()
	io.WriteString(w, string(resB))
}

func mixerTopStreams() (res string) {
	ctx := context.Background()
	conf := &oauth2.Config{
		ClientID:     mixer.ClientID,
		ClientSecret: mixer.ClientSecret,
		Scopes:       []string{"channel:details:self"},
		Endpoint: oauth2.Endpoint{
			AuthURL:  mixer.UrlAuthentication,
			TokenURL: mixer.UrlToken,
		},
	}

	// Redirect user to consent page to ask for permission
	// for the scopes specified above.
	url := conf.AuthCodeURL("state", oauth2.AccessTypeOffline)
	fmt.Printf("Visit the URL for the auth dialog: %v", url)

	// Use the authorization code that is pushed to the redirect
	// URL. Exchange will do the handshake to retrieve the
	// initial access token. The HTTP Client returned by
	// conf.Client will refresh the token as necessary.
	var code string
	if _, err := fmt.Scan(&code); err != nil {
		log.Fatal(err)
	}
	tok, err := conf.Exchange(ctx, code)
	if err != nil {
		log.Fatal(err)
	}
	res = tok.AccessToken
	return res
	//client := conf.Client(ctx, tok)
	//client.Get("...")
}

func twitchTopStreams() {
	
}

func youtubeTopStreams() {
	
}


//----- SEARCH IN SNETWORKS -----//
func searchStreams() {
	//https://mixer.com/api/v1/suggestions/channels?app=MixerAppFollowing&locale=en-US&country=US&timezone=PST&limit=3&device=Web
}
func searchInMixer(games []game, languages []string, countries []string, minimumViews int, maximumViews int) {
	// https://dev.mixer.com/rest/index.html
}
func searchInTwitch(games []game, languages []string, countries []string, minimumViews int, maximumViews int) {
	// DOC https://dev.twitch.tv/docs/api/reference

	// Get game by name
	// GET https://api.twitch.tv/helix/games

	// Get game analytics
	// GET https://api.twitch.tv/helix/analytics/games

	// Get streams by game
	// GET https://api.twitch.tv/helix/streams

}
func searchInYoutube(games []game, languages []string, countries []string, minimumViews int, maximumViews int) {
}
//func searchInFacebook(games []game, languages []string, countries []string, minimumViews int, maximumViews int) {// No API support}



func main() {
	handleRequests()
}
