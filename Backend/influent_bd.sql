-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-06-2020 a las 19:41:48
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `influent_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`) VALUES
(1, 'Health', 'bla'),
(2, 'Style', 'bla'),
(3, 'Videogames', 'bla'),
(4, 'Cuisine', 'bla'),
(5, 'Travels', 'bla'),
(6, 'Fitness', 'bla'),
(7, 'Esports', 'bla'),
(8, 'Comedy', 'bla'),
(9, 'Vlogs', 'bla'),
(10, 'Beauty', 'bla'),
(11, 'Films', 'bla'),
(12, 'Comics', 'bla'),
(13, 'Music', 'bla');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories_snetworks`
--

CREATE TABLE `categories_snetworks` (
  `id` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_snetwork` int(11) NOT NULL,
  `ref` int(11) DEFAULT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `name` varchar(24) NOT NULL,
  `img` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `companies`
--

INSERT INTO `companies` (`id`, `name`, `img`) VALUES
(1, 'Riot  Games', ''),
(2, 'Respawn Entertainment', ''),
(3, 'Electronic Arts', ''),
(4, 'Sony Computer Entertainm', ''),
(5, 'Naughty Dog', ''),
(6, 'Mojang AB', ''),
(7, 'Xbox Game Studios', ''),
(8, 'Valve Corporation', ''),
(9, 'Steam', ''),
(10, 'Blizzard Entertainment', ''),
(11, 'Epic Games', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `ISO` varchar(2) NOT NULL,
  `name` varchar(32) NOT NULL,
  `img` varchar(512) NOT NULL,
  `people` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `ISO`, `name`, `img`, `people`) VALUES
(1, 'US', 'USA', 'https://www.nationalflags.shop/WebRoot/vilkasfi01/Shops/2014080403/53E6/2F4B/DA0B/DC3B/C4B8/0A28/100B/0434/Flag_of_the_United_States.png', '327.7M'),
(2, 'ES', 'Spain', 'http://www.nationalflags.shop/WebRoot/vilkasfi01/Shops/2014080403/53E6/2F67/991E/BC90/1C32/0A28/100B/04AF/Flag_of_Spain.png', '46.66M'),
(3, 'MX', 'Mexico', 'https://www.nationalflags.shop/WebRoot/vilkasfi01/Shops/2014080403/53E6/2F5D/E073/38BC/6630/0A28/100B/04C0/Flag_of_Mexico_ml.png', '129.2M');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `games`
--

CREATE TABLE `games` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `img` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `games`
--

INSERT INTO `games` (`id`, `name`, `img`) VALUES
(1, 'League of Legends', ''),
(2, 'Apex Legends', ''),
(3, 'Overwatch', ''),
(4, 'Fornite', ''),
(5, 'Minecraft', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `games_platform`
--

CREATE TABLE `games_platform` (
  `game` int(11) NOT NULL,
  `platform` int(11) NOT NULL,
  `developer` int(11) NOT NULL,
  `publisher` int(11) NOT NULL,
  `release_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `games_platform`
--

INSERT INTO `games_platform` (`game`, `platform`, `developer`, `publisher`, `release_date`) VALUES
(3, 1, 10, 10, '2016-05-24'),
(3, 2, 10, 10, '2016-05-24'),
(3, 6, 10, 10, '2016-05-24'),
(4, 1, 11, 11, '2017-09-26'),
(4, 2, 11, 11, '2017-09-26'),
(4, 4, 11, 11, '2017-09-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `game_genres`
--

CREATE TABLE `game_genres` (
  `id` int(11) NOT NULL,
  `game` int(11) NOT NULL,
  `genre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `game_genres`
--

INSERT INTO `game_genres` (`id`, `game`, `genre`) VALUES
(1, 2, 1),
(2, 2, 3),
(3, 2, 4),
(4, 2, 10),
(5, 2, 34),
(6, 3, 3),
(7, 3, 4),
(8, 3, 34);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genres`
--

CREATE TABLE `genres` (
  `id` int(11) NOT NULL,
  `name` varchar(24) NOT NULL,
  `short_name` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `genres`
--

INSERT INTO `genres` (`id`, `name`, `short_name`) VALUES
(1, 'action', ''),
(2, 'Platform', ''),
(3, 'Shooter', ''),
(4, 'First Person Shooter', 'FPS'),
(5, 'Fighting', ''),
(6, 'Beat em up', ''),
(7, 'Stealth', ''),
(8, 'Survival', ''),
(9, 'Survival Horror', ''),
(10, 'Battle Royale', ''),
(11, 'Rhythm', ''),
(12, 'Metroidvania', ''),
(13, 'Role Playing Games', 'RPG'),
(14, 'Action Role Playing Game', 'Action RPG'),
(15, 'Massively Multiplayer On', 'MMORPG'),
(16, 'Tactical Role Playing Ga', 'Tactical RPG'),
(17, 'Sandbox Role Playing Gam', 'Sandbox RPG'),
(18, 'Japanese Role Playing Ga', 'JRPG'),
(19, 'Simulation', ''),
(20, 'Construction and Managem', ''),
(21, 'Life Simulation', ''),
(22, 'Vehicle Simulation', ''),
(23, 'Strategy', ''),
(24, 'Auto Battler', 'Auto Chess'),
(25, 'Multiplayer Online Battl', 'MOBA'),
(26, 'Real Time Strategy', 'RTS'),
(27, 'Real Time Tactics', 'RTT'),
(28, 'Tower Defense', ''),
(29, 'Turn Based Tactics', 'TBT'),
(30, 'Wargame', ''),
(31, 'Grand Strategy Wargame', ''),
(32, 'Sports', ''),
(33, 'Racing', ''),
(34, 'Competitive', ''),
(35, 'Card Game', ''),
(36, 'Massively Multiplayer On', 'MMO'),
(37, 'Mobile Game', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `img` varchar(512) NOT NULL,
  `countries` int(11) NOT NULL,
  `speakers` varchar(24) NOT NULL,
  `iso` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `languages`
--

INSERT INTO `languages` (`id`, `name`, `img`, `countries`, `speakers`, `iso`) VALUES
(1, 'spanish', 'https://img.freepik.com/vector-gratis/ilustracion-bandera-espana_53876-18168.jpg?size=626&ext=jpg', 20, '400M', 'es'),
(2, 'english', 'https://upload.wikimedia.org/wikipedia/commons/a/ae/Flag_of_the_United_Kingdom.svg', 57, '500M', 'en');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platforms`
--

CREATE TABLE `platforms` (
  `id` int(11) NOT NULL,
  `name` varchar(24) NOT NULL,
  `img` text NOT NULL,
  `release_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `platforms`
--

INSERT INTO `platforms` (`id`, `name`, `img`, `release_date`) VALUES
(1, 'Playstation 4', '', '2013-11-15'),
(2, 'Xbox One', '', '2013-11-22'),
(3, 'Steam', '', '2003-09-11'),
(4, 'Epic Games Store', '', '2018-12-06'),
(5, 'Twitch', '', '2017-01-01'),
(6, 'Battle.net', '', '1997-01-01'),
(7, 'Origin', '', '2011-06-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `snetworks`
--

CREATE TABLE `snetworks` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `url` varchar(512) NOT NULL,
  `img` varchar(512) NOT NULL,
  `users` varchar(24) NOT NULL,
  `description` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `snetworks`
--

INSERT INTO `snetworks` (`id`, `name`, `url`, `img`, `users`, `description`) VALUES
(1, 'Mixer', 'https://twitter.com/home', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJFwQECJZnI7DNj0avIoJXULch41G-OnMt1ldvbEg-OTmSVlO8&s', '1.300M', 'Microblogging service.'),
(2, 'Twitch', 'https://www.twitch.tv/', 'http://pngimg.com/uploads/twitch/twitch_PNG49.png', '3M/monthly', 'Video streaming.'),
(3, 'Facebook Gaming', 'https://www.instagram.com/', 'https://img.freepik.com/vector-gratis/instagram-nuevo-icono_1057-2227.jpg?size=338&ext=jpg', '800M', 'Social network focused on image and video.'),
(4, 'Youtube Gaming', 'https://www.youtube.com/', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSX1zdY5w5XGE3kJrjuskBnsbQvPcDdv6WMcBrrGITa4lCQxMbC&s', '1900M', 'Website focused on video sharing.');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categories_snetworks`
--
ALTER TABLE `categories_snetworks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `games_platform`
--
ALTER TABLE `games_platform`
  ADD PRIMARY KEY (`game`,`platform`);

--
-- Indices de la tabla `game_genres`
--
ALTER TABLE `game_genres`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `platforms`
--
ALTER TABLE `platforms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `snetworks`
--
ALTER TABLE `snetworks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `categories_snetworks`
--
ALTER TABLE `categories_snetworks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `games`
--
ALTER TABLE `games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `game_genres`
--
ALTER TABLE `game_genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `platforms`
--
ALTER TABLE `platforms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `snetworks`
--
ALTER TABLE `snetworks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
