import React, { Component } from 'react'
import TopVideo from './TopVideo'
import './styles/ResultsTop.css';

class ResultsTop extends Component { 
  render() {
    return (
        <div className="Result">
          <h1>Top Streams</h1>
          <TopVideo url="https://player.twitch.tv/?channel=alexby11" streamer="Alexby11" category="Games" viewers="10k"/>
          <TopVideo url="https://mixer.com/embed/player/Ninja?disableLowLatency=1" streamer="Ninja" category="Games" viewers="50k"/>
          <TopVideo url="https://player.twitch.tv/?channel=alexby11" streamer="Alexby11" category="Games" viewers="10k"/>
        </div>
    )
  }
}

export default ResultsTop