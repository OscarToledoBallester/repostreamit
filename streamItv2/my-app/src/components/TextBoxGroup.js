import React, { Component } from 'react'
import Selectize from 'react-selectize'
import './styles/TextBoxGroup.css';

class TextBoxGroup extends Component {
  render() {
    var self = this, 
            options = this.props.games.map(function(fruit){
                return {label: fruit, value: fruit}
            });
        return React.createElement(Selectize.MultiSelect, {
            options: options, 
            placeholder: "Select " + this.props.name, 
            transitionEnter: true, 
            transitionLeave: true}
        )
  }
}

export default TextBoxGroup