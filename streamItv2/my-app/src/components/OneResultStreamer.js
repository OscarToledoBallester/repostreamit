import React, { Component } from 'react'
import Accordion from 'react-bootstrap/Accordion'
import Card, {Button} from 'react-bootstrap/Card'
import './styles/OneResultStreamer.css';
import OneResultVideo from './OneResultVideo'
import StreamerDetails from './StreamerDetails'
import StreamerSocial from './StreamerSocial'
import StreamerGames from './StreamerGames'
import ZoomVideo from './ZoomVideo'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class OneResultStreamer extends Component {
    render() {
        let listItems = this.props.videoUrl.map((video) => <OneResultVideo name={this.props.name} platform={video.platform} url={video.url} size={this.props.videoUrl.length} /> );
        let listZooms = this.props.videoUrl.map((video) => 
            <Accordion.Collapse eventKey={`${video.platform}-${this.props.name}`}>
                <Card.Body><ZoomVideo name={`${video.platform}-${this.props.name}`} video={video.url} chat={video.chat} /></Card.Body>
            </Accordion.Collapse>
        );
        return (
            <Accordion>
                <Card>
                    <div className={`OneResultStreamer ${this.props.name}`}>
                        <div className="VideoResultPart logo">
                            <img className="Online" alt={this.props.name} src={this.props.logo} />
                            <StreamerSocial className="socialmedia" social={this.props.social} />
                        </div>
                        <div className="VideoResultPart details">
                            <StreamerDetails name={this.props.name} views={this.props.views} online={this.props.online} lastDate={this.props.lastDate} platforms={this.props.platforms}/>
                            <Accordion.Toggle as={Button} className="MyButtonDrop" variant="link" eventKey={`games-${this.props.name}`}>
                                Games <FontAwesomeIcon icon="chevron-down" />
                            </Accordion.Toggle>
                        </div>
                        <div className="VideoResultPart videos">{listItems}</div>
                        <Accordion.Collapse eventKey={`games-${this.props.name}`}>
                            <Card.Body>
                                <StreamerGames games={this.props.games} />
                            </Card.Body>
                        </Accordion.Collapse>
                        {listZooms}                
                    </div>
                </Card>
            </Accordion>
        )
  }
}

export default OneResultStreamer