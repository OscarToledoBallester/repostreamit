import React, { Component } from 'react'

import './styles/ResultsVideo.css';
import OneResultStreamer from './OneResultStreamer'

class ResultsVideo extends Component {
  render() {
    return (
        <div className="ResultsVideo">
            <h1>Results</h1>
            <OneResultStreamer name="Alexby" logo="https://uploads.mixer.com/avatar/2evg7cnb-101052282.jpg?width=128&height=128" games={[{"name":"League of Legends", "img":"https://i.pinimg.com/originals/c5/6c/77/c56c774ee09e3e16bf12460dea765109.jpg"},{"name":"Apex","img":"https://www.mundogamers.com/new/img/fichs/2423/platforms/big/5c594ef2-1c88-4a7e-9347-27ccb2217404.jpg"}]} views="1000" lastDate="30/03/2020" online="true" platforms={["Mixer", "Twitch"]} videoUrl={[{"platform":"Mixer","url":"https://mixer.com/embed/player/Ninja?disableLowLatency=1","chat":"https://mixer.com/embed/chat/Halo_Vic"},{"platform":"Twitch","url":"https://player.twitch.tv/?channel=alexby11","chat":"https://www.twitch.tv/embed/dementardo/chat"}]} social={[{"name":"twitter","url":"https://twitter.com/aLexBY11?s=20"},{"name":"instagram","url":"https://www.instagram.com/alexby11yt/?hl=es"}]}/>
            <OneResultStreamer name="Ninja" logo="https://uploads.mixer.com/avatar/2evg7cnb-101052282.jpg?width=128&height=128" games={[{"name":"Minecraft", "img":"https://www.mundogamers.com/new/img/fichs/173/platforms/big/4fadb167-e458-4cf0-a143-6dfc2e69771f.jpg"},{"name":"Fortnite","img":"https://cdn-products.eneba.com/resized-products/ZDY6Wiq_390x400_1x-0.jpg"}]} views="1000" lastDate="30/03/2020" online="true" platforms={["Mixer", "Twitch"]} videoUrl={[{"platform":"Mixer","url":"https://mixer.com/embed/player/Ninja?disableLowLatency=1","chat":"https://mixer.com/embed/chat/Halo_Vic"},{"platform":"Twitch","url":"https://player.twitch.tv/?channel=alexby11","chat":"https://www.twitch.tv/embed/dementardo/chat"}]} social={[{"name":"twitter","url":"https://twitter.com/Ninja?s=20"},{"name":"instagram","url":"https://www.instagram.com/ninja/?hl=es"}]}/>
        </div>
    )
  }
}

export default ResultsVideo