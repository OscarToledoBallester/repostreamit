import React, { Component } from 'react'
import './styles/ZoomVideo.css';

class ZoomVideo extends Component {
  render() {
    return (
        <div className="ZoomVideo">
            <div className="VideoContainer">
                <div className="VideoWrapper">
                    <iframe className="ZoomVideoIframe" src={this.props.video} frameborder="0" allowfullscreen="false" scrolling="no" height="378" width="620"></iframe>
                </div>
            </div>
            <div className="ChatContainer">
                <iframe className="ZoomChatIframe" title={`${this.props.name}'s chat frame`} i18n-title={`${this.props.name}'s chat frame`} allowfullscreen="false" src={this.props.chat} width="378" height="620"> </iframe>
            </div>
        </div>
    )
  }
}

export default ZoomVideo