import React, { Component } from 'react'
import './styles/InitialImg.css';
import logo from './imgs/logo.png';

class InitialImg extends Component {
  render() {
    return (
        <div className="Screen">
          <img alt="logo-streamit" src={logo} />
        </div>
    )
  }
}

export default InitialImg