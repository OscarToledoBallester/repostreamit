import React, { Component } from 'react'
import './styles/SideMenuFilters.css';
import TextBoxGroup from './TextBoxGroup';
import NumbersFromTo from './NumbersFromTo';

class SideMenuFilters extends Component {
  render() {
    return (
        <div className="SideMenuFilters">
          <h2>Filters</h2>
          <h3>Stream Platform</h3>
          <TextBoxGroup name="Stream Platform" games={["Mixer", "Twitch"]} />
          <h3>Language</h3>
          <TextBoxGroup name="Language" games={["Spanish", "English"]} />
          <h3>Country</h3>
          <TextBoxGroup name="Country" games={["Spain", "USA", "Mexico"]} />
          <h3>Viewers</h3>
          <NumbersFromTo />
        </div>
    )
  }
}

export default SideMenuFilters